module TaxonsHelper

  BOOLEAN_ATTRIBUTES = %w(disabled readonly multiple checked autobuffer
                             autoplay controls loop selected hidden scoped async
                             defer reversed ismap seemless muted required
                             autofocus novalidate formnovalidate open pubdate).to_set
        BOOLEAN_ATTRIBUTES.merge(BOOLEAN_ATTRIBUTES.map {|attribute| attribute.to_sym })

  def breadcrumbs(taxon, separator="&nbsp;&raquo;&nbsp;")
    return "" if current_page?("/") || taxon.nil?
    separator = raw(separator)
    crumbs = [content_tag(:li, link_to(t(:home), root_path) + separator)]
    if taxon
      crumbs << content_tag(:li, link_to(t(:products), articles_path) + separator)
      crumbs << taxon.ancestors.collect { |ancestor| content_tag(:li, link_to(ancestor.name, seo_url(ancestor)) + separator) } unless taxon.ancestors.empty?
      crumbs << content_tag(:li, content_tag(:span, link_to(taxon.name, seo_url(taxon))))
    else
      crumbs << content_tag(:li, content_tag(:span, t(:products)))
    end
    crumb_list = content_tag(:ul, raw(crumbs.flatten.map { |li| li.mb_chars }.join), :class => 'inline')
    content_tag(:div, crumb_list, :id => 'breadcrumbs')
  end

  # Returns an HTML block tag of type +name+ surrounding the +content+. Add
  # HTML attributes by passing an attributes hash to +options+.
  # Instead of passing the content as an argument, you can also use a block
  # in which case, you pass your +options+ as the second parameter.
  # Set escape to false to disable attribute value escaping.
  #
  # ==== Options
  # The +options+ hash is used with attributes with no value like (<tt>disabled</tt> and
  # <tt>readonly</tt>), which you can give a value of true in the +options+ hash. You can use
  # symbols or strings for the attribute names.
  #
  # ==== Examples
  #   content_tag(:p, "Hello world!")
  #    # => <p>Hello world!</p>
  #   content_tag(:div, content_tag(:p, "Hello world!"), :class => "strong")
  #    # => <div class="strong"><p>Hello world!</p></div>
  #   content_tag("select", options, :multiple => true)
  #    # => <select multiple="multiple">...options...</select>
  #
  #   <%= content_tag :div, :class => "strong" do -%>
  #     Hello world!
  #   <% end -%>
  #    # => <div class="strong">Hello world!</div>
  def content_tag(name, content_or_options_with_block = nil, options = nil, escape = true, &block)
    if block_given?
      options = content_or_options_with_block if content_or_options_with_block.is_a?(Hash)
      content_tag_string(name, capture(&block), options, escape)
    else
      content_tag_string(name, content_or_options_with_block, options, escape)
    end
  end


  def content_tag_string(name, content, options, escape = true)
    tag_options = tag_options(options, escape) if options
    "<#{name}#{tag_options}>#{escape ? ERB::Util.h(content) : content}</#{name}>".html_safe
  end

  def tag_options(options, escape = true)
    unless options.blank?
      attrs = []
      options.each_pair do |key, value|
        if key.to_s == 'data' && value.is_a?(Hash)
          value.each do |k, v|
            if !v.is_a?(String) && !v.is_a?(Symbol)
              v = v.to_json
            end
            v = ERB::Util.html_escape(v) if escape
            attrs << %(data-#{k.to_s.dasherize}="#{v}")
          end
        elsif BOOLEAN_ATTRIBUTES.include?(key)
          attrs << %(#{key}="#{key}") if value
        elsif !value.nil?
          final_value = value.is_a?(Array) ? value.join(" ") : value
          final_value = ERB::Util.html_escape(final_value) if escape
          attrs << %(#{key}="#{final_value}")
        end
      end
      " #{attrs.sort * ' '}".html_safe unless attrs.empty?
    end
  end
end
