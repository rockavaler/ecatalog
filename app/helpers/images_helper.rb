module ImagesHelper

  Image.attachment_definitions[:attachment][:styles].each do |style, v|
        define_method "#{style}_image" do |article, *options|
          options = options.first || {}
          if article.images.empty?
            image_tag "noimage/#{style}.png", options
          else
            image = article.images.first
            options.reverse_merge! :alt => image.alt.blank? ? article.name : image.alt
            image_tag image.attachment.url(style), options
          end
        end
  end

end
