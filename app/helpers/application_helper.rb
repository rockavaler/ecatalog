module ApplicationHelper
    # generates nested url to product based on supplied taxon
  def seo_url(taxon)
    return  nested_taxons_path(taxon.permalink) #if product.nil?
    warn "DEPRECATION: the /t/taxon-permalink/p/product-permalink urls are "+
      "not used anymore. Use product_url instead. (called from #{caller[0]})"
    #return product_url(product)
  end
  def is_active?(current_controller_name)
    current_controller_name.include?(controller.controller_name.to_sym)
  end
  def title(page_title)
    content_for (:title) { page_title }
  end
end
