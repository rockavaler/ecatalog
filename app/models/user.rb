class User < ActiveRecord::Base
	

  attr_accessible :email, :password, :password_confirmation, :idno, :description, :contact_person, :phone_number, :url, :name
  
  attr_accessor :password
  before_save :encrypt_password

  has_many :locations
  has_many :promotions
  has_many :articles, :order => 'published_at DESC, title ASC',
                      :dependent => :nullify
  
  validates_confirmation_of :password
  validates_presence_of :password, :on => :create
  validates_presence_of :email
  validates_uniqueness_of :email
  validates_presence_of :contact_person,:phone_number, :name


  #Scopes
  scope :activated, where(:activated => true)

  make_permalink

  def self.authenticate(email, password)
    user = find_by_email(email)
    if user && user.password_hash == BCrypt::Engine.hash_secret(password, user.password_salt)
      user
    else
      nil
    end
  end
  
  def encrypt_password
    if password.present?
      self.password_salt = BCrypt::Engine.generate_salt
      self.password_hash = BCrypt::Engine.hash_secret(password, password_salt)
    end
  end

  def activate!
      self.activated = true
      self.save!
  end

  def generate_login_code!
    self.login_token = ActiveSupport::SecureRandom.hex(20)
    self.login_token_generated_at = Time.zone.now
    self.save(:validate => false)
  end

  def send_activation_email
    self.generate_login_code!
    UserMailer.activation_email(self).deliver
  end
  def send_password_reset
    self.generate_login_code!
    UserMailer.password_reset(self).deliver
  end

  def is_login_token_valid?(token)
    (token == self.login_token)
  end

  def to_param
      permalink.present? ? permalink : (permalink_was || name.to_s.to_url)
  end

  def special?
   user_type==1
  end
end


