class Ticket < ActiveRecord::Base
  belongs_to :ticket_batch


  scope :not_extracted,  joins('left outer join extractions on tickets.id=extractions.ticket_id ').where('extractions.ticket_id is null and tickets.registered_at is not null ')

  def registered?
    !registered_at.nil?
  end

end
