class Promotion < ActiveRecord::Base
belongs_to :user
has_many :ticket_batches
has_many :tickets , :through => :ticket_batches
has_many :extractions

before_validation :generate_permalink, :on => :create

def to_param
  permalink
end
def started?
  Time.current > self.starts_at.beginning_of_day
end
def ended?
  Time.current > self.ends_at.end_of_day
end

private
def generate_permalink
  return self.permalink  unless self.permalink.blank?
  record = true
  while record
    random = "#{Array.new(8){rand(0..9)}.join}#{Time.now.strftime("%y")}"
    record = self.class.where(:permalink  => random).first
  end
  self.permalink = random
end
end
