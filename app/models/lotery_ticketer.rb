require 'prawn'
class LoteryTicketer

   @promotion
   @ticket_batch
  def generate_tickets( ticket_batch )
    @promotion = ticket_batch.promotion
    @ticket_batch = ticket_batch
    for i in 1..ticket_batch.tickets_count
      ticket_batch.tickets.build(:ticket_number => generate_ticket_number)
      puts "Value of local variable is #{i}"
    end
    ticket_batch.save
    makepdf
  end

  def generate_string(length=10)
    chars = '234679ACDEFGHJKMNPQRTVWXYZ'
    random_string = ''
    length.times {random_string<<chars[rand(chars.size)]}
    random_string
  end

  def generate_ticket_number

    begin
    unique_ticket_number = generate_string(5)
    end while (@promotion.tickets.exists?(:ticket_number =>unique_ticket_number) ||
              @ticket_batch.tickets.map(& :ticket_number).include?(unique_ticket_number)  )

    unique_ticket_number
  end

  def makepdf
    filename ="#{Rails.root}/tmp/#{generate_string(8).downcase}.pdf"
    pdf = Prawn::Document.new(:page_size => "A4", :page_layout => :portrait, :margin => 22)
    pdf.font_families.update("Verdana" => {
        :bold => "#{Rails.root}/app/prawn_fonts/verdanab.ttf",
        :italic => "#{Rails.root}/app/prawn_fonts/verdanai.ttf",
        :normal  => "#{Rails.root}/app/prawn_fonts/verdana.ttf" })
    pdf.font  'Verdana'

    pdf.define_grid(:columns => 8, :rows => 5, :column_gutter => 4, :row_gutter => 4)
    #pdf.grid.show_all
    i = 0
    company_name =  normalize(@ticket_batch.promotion.user.name,40)
    promotion_name   = normalize(@ticket_batch.promotion.name,40)
    promotion_id = @ticket_batch.promotion.permalink
    ends_at =     @ticket_batch.promotion.ends_at
    starts_at =    @ticket_batch.promotion.starts_at
    register_text = I18n.t('tickets.register_ticket', :date_from=>starts_at.strftime('%d/%m/%y'), :date_to=>ends_at.strftime('%d/%m/%y'))
    @ticket_batch.tickets.each do |ticket|
        pos = i % 40                    # pos = label's position on the page (0-39)

        box = pdf.grid(pos / 8, pos % 8)    # lay labels out in 4 columns
                                        # (print label in box)
        #pdf.start_new_page if pos == 0

        pdf.stroke_rounded_rectangle box.top_left, box.width, box.height, 10
        pdf.bounding_box box.top_left, :width => box.width, :height => box.height do
          pdf.draw_text "www.ecatalog.md/promotions/#{promotion_id}", :at => [60,10], :size => 4,:rotate=>-270, :style => :bold
          pdf.draw_text ticket.ticket_number, :at => [10,140],  :style => :bold, :size =>11
          pdf.draw_text company_name, :at => [10,20], :rotate=>-270, :size => 6
          pdf.draw_text normalize( I18n.t('tickets.registration_invitation'),40), :at => [16,20], :rotate=>-270, :size => 6
          pdf.draw_text promotion_name , :at => [24,15], :rotate=>-270, :style => :bold, :size => 6
          pdf.draw_text  I18n.t('tickets.good_luck') , :at => [38,20], :rotate=>-270, :style => :bold, :size => 10
          pdf.draw_text register_text , :at => [55,10], :rotate=>-270,  :size => 3.5

        end


        i += 1
    end
    pdf.render_file(filename)

    pdf_file = File.open(filename)

    @ticket_batch.pdf = pdf_file
    @ticket_batch.save!
    pdf_file.close
    File.delete(filename)
  end

   private

   def normalize(text, maxlength)
     if text.length>maxlength
       text = text[0..maxlength-4]+"..."
     end
     text.center(maxlength)
   end

   def split_in_lines(text,nbLines)
     ar = []
     start_index = 0
     max_length = 10
     i = 0
     nbLines.times do
       ar <<""
     end
     has_next_line = text.length>0
     while has_next_line do
       rlimit =  [start_index+max_length,text.length].min
       end_index = text[0..rlimit].rindex(" ")
       if end_index.nil?
         end_index = text.length
       else
         end_index = end_index -1
       end

       if i== nbLines-1  || end_index <=start_index
         end_index=text.length
       end
       #puts "start index-#{start_index} end index-#{end_index} "
       ar[i]=normalize(text[start_index..end_index].strip,max_length )
       start_index = end_index+1
       has_next_line = text.length>start_index+max_length
       i = i+1
     end
     ar
   end
end
