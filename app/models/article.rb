class Article < ActiveRecord::Base

	validates :name, :presence => true
	validates :price, :presence => true
	
	belongs_to :user
	  
	has_many :images, :as => :viewable, :order => :position, :dependent => :destroy
	has_and_belongs_to_many :taxons
  has_and_belongs_to_many :article_groups
  has_and_belongs_to_many :locations

    include ::Scopes::Article
   #SCOPES
  scope :not_deleted,     where("articles.deleted_at is NULL")

  scope :available,       lambda { |*on| where("articles.available_on <= ? OR 1=1 ", on.first || Time.zone.now ) }
  scope :active,          lambda{ not_deleted.available }
  scope :new_arrivals,    lambda { |*on| where("articles.created_at > ?", on.first || 1.weeks.ago ) }
	
	  def featured_image(image_size = :small)
      images.first ? images.first.attachment.url(image_size) : "no_image_#{image_size.to_s}.jpg"
    end
  
  make_permalink

  # @param [Object] search
  def self.search(search)
     if search
           where('name LIKE ?', "%#{search}%")
         else
          scoped
     end
   end

  def to_param
    permalink.present? ? permalink : (permalink_was || name.to_s.to_url)
  end


end
