class Taxon < ActiveRecord::Base
  private
  # open access to modify the id
  # used on load taxons
  def self.attributes_protected_by_default
            # default is ["id", "type"]
            []
    end

   public
  #before_save :set_permalink

  acts_as_nested_set :dependent => :destroy

  belongs_to :taxonomy
  has_and_belongs_to_many :articles
  
  validates :name, :presence => true
  make_permalink

  def set_permalink
       if parent_id.nil?
         self.permalink = name.to_url if self.permalink.blank?
       else
         parent_taxon = Taxon.find(parent_id)
         self.permalink = [parent_taxon.permalink, (self.permalink.blank? ? name.to_url : self.permalink.split('/').last)].join('/')
       end
  end

  def to_param
       permalink.present? ? permalink : (permalink_was || name.to_s.to_url)
  end

end
