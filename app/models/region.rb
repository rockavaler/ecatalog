class Region < ActiveRecord::Base
  belongs_to  :district
  has_many :locations
end
