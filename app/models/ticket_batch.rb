class TicketBatch < ActiveRecord::Base
  belongs_to :promotion
  has_many :tickets
  has_attached_file :pdf,
  :url  => "/ticketbatches/:id/:basename.:extension",
  :path => ":rails_root/ticketbatches/:id/:filename"

end
