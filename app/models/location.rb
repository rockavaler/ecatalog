class Location < ActiveRecord::Base
  belongs_to :user
  belongs_to :region

  def contact_information
    "#{address_line1}  #{address_line2} (#{region.name})"
  end
end
