class ApplicationController < ActionController::Base
	protect_from_forgery
	# before_filter :_reload_libs, :if => :_reload_libs?
	
	helper_method :current_user

	private
	def current_user
		@current_user ||= User.find(session[:user_id]) if session[:user_id]
	end
	
	
 
# def _reload_libs
#   RELOAD_LIBS.each do |lib|
#     require_dependency lib
#   end
# end
#  
# def _reload_libs?
#   defined? RELOAD_LIBS
# end

end
