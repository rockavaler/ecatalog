class ArticlesController < ApplicationController
  # GET /articles
  # GET /articles.xml
  def index
    @articles = Article.search(params[:search]).paginate(  :per_page => 12, :page => params[:page])

    respond_to do |format|
      format.html # index.html.erb
      format.json  { render :json => @articles }
    end

  end

  
  # GET /articles/1
  # GET /articles/1.xml
  def show
    @article = Article.find_by_permalink (params[:id])
    @locations =  @article.locations
    respond_to do |format|
      format.html # show.html.erb
      format.json  { render :json => @article }
    end
  end

end
