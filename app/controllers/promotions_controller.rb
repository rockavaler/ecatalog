class PromotionsController < ApplicationController
  def index
    @promotions = Promotion.all
    respond_to do |format|
      format.html # index.html.erb
      format.json  { render :json => @promotions }
    end
  end

  def show
    @promotion = Promotion.find_by_permalink!(params[:id])
    respond_to do |format|
      format.html # show.html.erb
      format.json  { render :json => @promotion }
    end
  end
end
