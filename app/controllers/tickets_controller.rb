class TicketsController < ApplicationController
  def register
    @promotion = Promotion.find_by_permalink!  (params[:promotion_id])
    if @promotion.started?  and !@promotion.ended?
      if request.post?
        @t = Ticket.new(params[:ticket])
        pattern = /^[a-zA-Z0-9._+-]+@[a-zA-Z0-9-]+\.[a-zA-Z0-9.]{2,5}$/
        if  @t.name.blank?  || @t.email.blank?  ||  @t.ticket_number.blank?  ||  !(@t.email.match pattern)
          @ticket = @t
          respond_to do |format|
            flash[:error] = 'Completati corect toate cimpurile necesare!'
            format.html { render :action => "register" }
          end
          return
        end
        @ticket1 = @promotion.tickets.find_by_ticket_number(@t.ticket_number.upcase)

        if    @ticket1

          if @ticket1.registered?
            respond_to do |format|
              flash[:alert] = "Ticketul cu numarul #{@ticket1.ticket_number}  e deja inregistrat."
              format.html { redirect_to promotion_path(@promotion) }
            end
            return
          end

          @ticket1.registered_at = Time.now
          @ticket1.name = @t.name
          @ticket1.email = @t.email
          @ticket1.save
          TicketMailer.deliver_registration_confirmation(@ticket1,@promotion)
          respond_to do |format|
            flash[:success] = 'Ticketul a fost inregistrat cu success.'
            format.html { redirect_to promotion_path(@promotion) }
          end
        else
          @ticket = @t
          respond_to do |format|
            flash[:error] = 'Nu exista un tichet cu acest numar'
            format.html { render :action => "register" }
          end
        end

      else
        @ticket = Ticket.new
        @promotion = Promotion.find_by_permalink! (params[:promotion_id])
        respond_to do |format|
          format.html { render :action => :register, :layout => !request.xhr? }
          format.html # new.html.erb
        end
      end
    else
      respond_to do |format|
        flash[:error] = 'Inregistrarea interzisa '
        format.html { redirect_to promotion_path(@promotion) }
      end
    end

  end

end
