class Accounts::TaxonomiesController < Accounts::BaseController
    def index
    @taxonomies = Taxonomy.all

    respond_to do |format|
      format.html # index.html.erb
      format.json  { render :json => @taxonomies }
    end
  end
  
  def new

        @taxonomy = Taxonomy.new

    respond_to do |format|
      
     format.html {render :action => :new, :layout => !request.xhr?}
     #format.html # new.html.erb
     #format.json  { render :json => @article }
    end
  end
   def edit
    @taxonomy = Taxonomy.find(params[:id])
  end
  
  def create
    @taxonomy = Taxonomy.new(params[:taxonomy])

    respond_to do |format|
      if @taxonomy.save
        format.html { redirect_to taxonomies_path}
          #(@taxonomy, :notice => 'taxonomy was successfully created.') }
    #    format.json  { render :json => @article, :status => :created, :location => @article }
      else
        format.html { render :action => "new" }
        format.json  { render :json => @taxonomy.errors, :status => :unprocessable_entity }
      end
    end
  end
  
  def destroy
  @taxonomy = Taxonomy.find(params[:id])
    @taxonomy.destroy

    respond_to do |format|
      format.html { redirect_to(taxonomies_url) }
      format.json  { head :ok }
    end
  end
  # reate.wants.html {redirect_to edit_admin_taxonomy_url(@taxonomy)}
  # update.wants.html {redirect_to collection_url}
# 
  # edit.response do |wants|
    # wants.html
    # wants.js do
      # render :partial => 'edit.html.erb'
    # end
  # end

  def get_children
    @taxons = Taxon.find(params[:parent_id]).children
  end
  
end
