class Accounts::ArticlesController < Accounts::BaseController
  before_filter :check_articles_limits, :only => [:new, :create]
  # GET /articles
  # GET /articles.xml
  def index
    @articles = current_user.articles.search(params[:search]).paginate(  :per_page => 12, :page => params[:page])

    respond_to do |format|
      format.html # index.html.erb
      format.json  { render :json => @articles }
    end
  end

  # GET /articles
  # GET /articles.xml
  def my_articles
    @articles = current_user.articles.all

    respond_to do |format|
      format.html # index.html.erb
      format.json  { render :json => @articles }
    end
  end

  # GET /articles/1
  # GET /articles/1.xml
  def show
    @article = current_user.articles.find_by_permalink!(params[:id])

    respond_to do |format|
      format.html # show.html.erb
      format.json  { render :json => @article }
    end
  end

  # GET /articles/new
  # GET /articles/new.xml
  def new
    @article = Article.new


    respond_to do |format|
    	
     format.html {render :action => :new, :layout => !request.xhr?}
   #  format.html # new.html.erb
    format.json  { render :json => @article }
    end
  end

  # GET /articles/1/edit
  def edit
  	@article = current_user.articles.find_by_permalink!(params[:id])
  end

  # POST /articles
  # POST /articles.xml
  def create
    @article = current_user.articles.new(params[:article])

    respond_to do |format|
      if @article.save
        format.html { redirect_to edit_accounts_article_path(@article, :notice => 'Article was successfully created.') }
    #    format.json  { render :json => @article, :status => :created, :location => @article }
      else
        format.html { render :action => "new" }
        format.json  { render :json => @article.errors, :status => :unprocessable_entity }
      end
    end
  end

  # PUT /articles/1
  # PUT /articles/1.xml
  def update
   @article = current_user.articles.find_by_permalink!(params[:id])
   params[:article][:location_ids] ||= []

    respond_to do |format|
      if @article.update_attributes(params[:article])
        format.html { redirect_to(accounts_article_path(@article), :notice => 'Article was successfully updated.') }
        format.json  { head :ok }
      else
        format.html { render :action => "edit" }
        format.json  { render :xml => @article.errors, :status => :unprocessable_entity }
      end
    end
  end

  # DELETE /articles/1
  # DELETE /articles/1.xml
  def destroy
 	@article = current_user.articles.find_by_permalink!(params[:id])
    @article.destroy

    respond_to do |format|
      format.html { redirect_to(articles_url) }
      format.json  { head :ok }
    end
  end

  private
  def check_articles_limits
    unless current_user.special?
      if current_user.articles.size > 2
        flash[:alert] = t(:reached_max_articles)
        redirect_to accounts_articles_path
      end
    end
  end
end
