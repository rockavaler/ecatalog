class Accounts::SessionsController < Accounts::BaseController
def new
end

def create
  user = User.authenticate(params[:email], params[:password])
  if user
    if  user.activated?
      session[:user_id] = user.id
      redirect_to accounts_index_path, :notice => "Logged in!"
      else
        flash.now.alert = "Contul dvs nu a fost activat. Verificati posta electronica!"
      render "new"
    end

  else
    flash.now.alert = "Invalid email or password"
    render "new"
  end
end

def destroy
  session[:user_id] = nil
  redirect_to root_url, :notice => "Logged out!"
end
end
