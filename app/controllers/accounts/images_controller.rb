
class Accounts::ImagesController < Accounts::BaseController
  before_filter :check_article_images_limits, :only => [:new, :create]
#	before_filter :load_data

 def index
 	  @article = current_user.articles.find_by_permalink(params[:article_id])
 	  respond_to do |format|
 	        format.html # index.html.erb
 	        # format.json  { render :json => @article }
 	      end
 end	

def new
  @image = Image.new
  @article = current_user.articles.find_by_permalink(params[:article_id])
  respond_to do |format|
      
     format.html {render :action => :new, :layout => !request.xhr?}
     format.html # new.html.erb
     #format.json  { render :json => @article }
    end
end

def create
  image_data = params[:image]
  @article = current_user.articles.find_by_permalink(params[:article_id])
  @image = @article.images.build(image_data)
  
   respond_to do |format|
      if @image.save
        flash[:success] = "Photo successfully uploaded"
        format.html { redirect_to accounts_article_images_url(@article) }
      else
        format.html { render :action => "new" }
      end
    end
end

 def edit
   @article = current_user.articles.find_by_permalink(params[:article_id])
   @image = Image.find(params[:id])

 end

 def destroy

     @article = current_user.articles.find_by_permalink!(params[:article_id])
     @image = Image.find(params[:id])
     @image.destroy
     flash[:success] = "Photo successfully deleted"
     respond_to do |format|
       format.html { redirect_to  accounts_article_images_url(@article) }
       format.js
      end
end


def update
  @article = current_user.articles.find_by_permalink!(params[:article_id])
  @image = Image.find(params[:id])
   image_data =  params[:image]

  respond_to do |format|
    if @image.update_attributes(image_data)
      puts "updated"

      format.html { redirect_to(accounts_article_images_url(@article), :notice => 'Imaginea a fost actualizata cu success!') }
      format.json  { head :ok }
    else
      puts "else"

      format.html { render :action => "edit" }
      format.json  { render :xml => @image.errors, :status => :unprocessable_entity }
    end
  end
end

  

  private

  def load_data
    @article = Article.find_by_permalink(params[:article_id])
  end

  def set_viewable

      object.viewable = @article
  end

  def destroy_before
    @viewable = object.viewable
  end

  def check_article_images_limits
    unless current_user.special?
      @article = current_user.articles.find_by_permalink!(params[:article_id])
      if @article.images.size > 2
        flash[:alert] = t(:reached_max_images)
        redirect_to accounts_article_images_path(@article)
      end
    end
  end

end
