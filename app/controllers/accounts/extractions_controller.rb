class Accounts::ExtractionsController < Accounts::BaseController
  def extract
    promotion = current_user.promotions.find_by_permalink!(params[:promotion_id])
    if promotion.extractions.count < promotion.extractions_count and promotion.ended?

      ticket = promotion.tickets.not_extracted.offset(rand(promotion.tickets.not_extracted.count)).first
      extraction = Extraction.new(:promotion => promotion, :ticket => ticket)
      #@extraction.ticket.( @ticket)
      #@extraction.promotion  = @promotion
      extraction.save
      #send winning mail
      TicketMailer.winner_notification(extraction.ticket,promotion).deliver
      respond_to do |format|
        flash[:success] = 'Extragere efectuata cu success.'
        format.html { redirect_to accounts_promotion_path(promotion) }
      end
    else
      respond_to do |format|
        flash[:alert] = 'Nu pot fi efectuate extrageri.'
        format.html { redirect_to accounts_promotion_path(promotion) }
      end
    end

  end
end
