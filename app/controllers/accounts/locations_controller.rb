class Accounts::LocationsController < Accounts::BaseController
  before_filter :check_locations_limits, :only => [:new, :create]

  def index
    @locations = current_user.locations

    respond_to do |format|
      format.html # index.html.erb
      format.json  { render :json => @locations }
    end
  end



  def show
      @location = current_user.locations.find(params[:id])

      respond_to do |format|
        format.html # show.html.erb
        format.json  { render :json => @location }
      end
    end

    # GET /locations/new
    # GET /locations/new.xml
    def new
      @location = Location.new
      respond_to do |format|

       format.html {render :action => :new, :layout => !request.xhr?}
     #  format.html # new.html.erb
      format.json  { render :json => @location }
      end

    end

    # GET /locations/1/edit
    def edit
    	@location = current_user.locations.find(params[:id])
    end

    # POST /locations
    # POST /locations.xml
    def create
      @location = current_user.locations.new(params[:location])

      respond_to do |format|
        if @location.save
          format.html { redirect_to accounts_locations_path( :notice => 'location was successfully created.') }
      #    format.json  { render :json => @location, :status => :created, :location => @location }
        else
          format.html { render :action => "new" }
          format.json  { render :json => @location.errors, :status => :unprocessable_entity }
        end
      end
    end

    # PUT /locations/1
    # PUT /locations/1.xml
    def update
     @location = current_user.locations.find(params[:id])

      respond_to do |format|
        if @location.update_attributes(params[:location])
          format.html { redirect_to(accounts_location_path(@location), :notice => 'location was successfully updated.') }
          format.json  { head :ok }
        else
          format.html { render :action => "edit" }
          format.json  { render :xml => @location.errors, :status => :unprocessable_entity }
        end
      end
    end

    # DELETE /locations/1
    # DELETE /locations/1.xml
    def destroy
   	@location = current_user.locations.find(params[:id])
      @location.destroy

      respond_to do |format|
        format.html { redirect_to(accounts_locations_url) }
        format.json  { head :ok }
      end
    end

  private
  def  check_locations_limits
   unless current_user.special?
       if current_user.locations.size > 2
         flash[:alert] = t(:reached_max_locations)
         redirect_to accounts_locations_path
       end
   end
  end

end
