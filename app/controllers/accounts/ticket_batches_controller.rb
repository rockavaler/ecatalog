class Accounts::TicketBatchesController <   Accounts::BaseController
  def new
    #@ti = Image.new
    #@article = current_user.articles.find_by_permalink(params[:article_id])
    respond_to do |format|
      format.html {render :action => :new, :layout => !request.xhr?}
      format.html # new.html.erb
    end
  end
  def create
    @promotion = current_user.promotions.find_by_permalink!(params[:promotion_id])
    if @promotion.ended?
      respond_to do |format|
        flash[:alert] = 'Erroare creare tickete'
        format.html { redirect_to accounts_promotion_url(@promotion) }
      end
      return
    end
    unless current_user.special?
          if @promotion.ticket_batches.size > 2
            respond_to do |format|
              flash[:alert] = t(:reached_max_tickets )
              format.html { redirect_to accounts_promotion_url(@promotion) }
            end
            return
          end
    end
    @ticket_batch = @promotion.ticket_batches.new
    @ticket_batch.tickets_count = 40
    #@promotion.ticket_batches.build(@ticket_batch)


    respond_to do |format|
      if @ticket_batch.save
        ticketer = LoteryTicketer.new
        ticketer.generate_tickets(@ticket_batch)

        flash[:success] = "Batch successfully created. Please wait until it will be completed"
        format.html { redirect_to accounts_promotion_url(@promotion) }
      else
       format.html { render :action => "new" }
      end
    end
  end
  def download
    @promotion = current_user.promotions.find_by_permalink!(params[:promotion_id])
    @ticket_batch = @promotion.ticket_batches.find(params[:id])

    send_file(@ticket_batch.pdf.path,
              :filename => @ticket_batch.id.to_s + @ticket_batch.pdf_file_name,
              :type => "application/pdf")
  end
end
