class Accounts::UsersController < Accounts::BaseController
  def new
    @user = User.new
  end

  # show.before do
  #   @articles = @user.articles
  # end

  def edit
    @user = current_user
  end

  def update
    @user = current_user

    respond_to do |format|
      if @user.update_attributes(params[:user])
        format.html { redirect_to(edit_accounts_user_path(@user), :notice => 'user was successfully updated.') }
        format.json { head :ok }
      else
        format.html { render :action => "edit" }
        format.json { render :xml => @user.errors, :status => :unprocessable_entity }
      end
    end
  end

  def create
    @user = User.new(params[:user])
    if @user.save
      @user.send_activation_email
      redirect_to log_in_path, :notice => "Contul a fost creat cu suucess!"
    else
      render "new"
    end
  end



  def activate_account
    @user = User.find_by_permalink!(params[:id])
    if @user
      if @user.is_login_token_valid? params[:token]
        if @user.activated?
          flash[:success] = t('users.activation.already_done')
        else
          flash[:success] = t('users.activation.activated')
          @user.activate!
        end
      else
        flash[:error] = t('users.activation.invalid_html')
      end
    else
      flash[:error] = t('users.activation.invalid_user')
    end

    redirect_to log_in_path
  end

end
