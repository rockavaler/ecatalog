class Accounts::TaxonsController < Accounts::BaseController
  # include Railslove::Plugins::FindByParam::SingletonMethods
  # resource_controller
  #
  # before_filter :load_object, :only => [:selected, :available, :remove]
  # before_filter :load_permalink_part, :only => :edit
  # belongs_to :product, :taxonomy

  def index
    @article = current_user.articles.find_by_permalink(params[:article_id])
    @taxons = @article.taxons.all
    respond_to do |format|
      format.html { render :action => :selected } # index.html.erb
                                                  # format.json  { render :json => @article }
    end
  end


  def selected
    @taxons = @article.taxons
  end

#
  def available
    # @article = current_user.articles.find(params[:article_id])
    @article = Article.find_by_permalink!(params[:article_id])
    if params[:q].blank?
      @available_taxons = []
    else
      @available_taxons = Taxon.where('lower(name) LIKE ?', "%#{params[:q].mb_chars.downcase}%")
    end
    @available_taxons.delete_if { |taxon| @article.taxons.include?(taxon) }
    respond_to do |format|
      format.js {render :layout => false}
    end
    #
  end

#
  def remove
    @article = Article.find_by_permalink!(params[:article_id])
    @taxon = Taxon.find(params[:id])

    @article.taxons.delete(@taxon)
    @article.save
    @taxons = @article.taxons
    respond_to do |format|
      format.html {  redirect_to(accounts_article_taxons_path(@article), :notice => 'Categoria a fost retrasa.') }
    end
  end

#
  def select
    @article = Article.find_by_permalink!(params[:article_id])
    @taxon = Taxon.find(params[:id])
    @article.taxons << @taxon
    @article.save
    @taxons = @article.taxons
    respond_to do |format|
      format.js { render :layout => false }
    end

  end

#
# private
# def create_before
# @taxon.taxonomy_id = params[:taxonomy_id]
# end
# 
# def update_before
# parent_id = params[:taxon][:parent_id]
# new_position = params[:taxon][:position]
# 
# if parent_id || new_position #taxon is being moved
# new_parent = parent_id.nil? ? @taxon.parent : Taxon.find(parent_id.to_i)
# new_position = new_position.nil? ? -1 : new_position.to_i
# 
# # Bellow is a very complicated way of finding where in nested set we
# # should actually move the taxon to achieve sane results,
# # JS is giving us the desired position, which was awesome for previous setup,
# # but now it's quite complicated to find where we should put it as we have
# # to differenciate between moving to the same branch, up down and into
# # first position.
# new_siblings = new_parent.children
# if new_position <= 0 && new_siblings.empty?
# @taxon.move_to_child_of(new_parent)
# elsif new_parent.id != @taxon.parent_id
# if new_position == 0
# @taxon.move_to_left_of(new_siblings.first)
# else
# @taxon.move_to_right_of(new_siblings[new_position-1])
# end
# elsif new_position < new_siblings.index(@taxon)
# @taxon.move_to_left_of(new_siblings[new_position]) # we move up
# else
# @taxon.move_to_right_of(new_siblings[new_position]) # we move down
# end
# # Reset legacy position, if any extensions still rely on it
# new_parent.children.reload.each{|t| t.update_attribute(:position, t.position)}
# 
# if parent_id
# @taxon.reload
# @taxon.set_permalink
# @taxon.save!
# @update_children = true
# end
# end
# 
# if params.key? "permalink_part"
# parent_permalink = @taxon.permalink.split("/")[0...-1].join("/")
# parent_permalink += "/" unless parent_permalink.blank?
# params[:taxon][:permalink] = parent_permalink + params[:permalink_part]
# end
# #check if we need to rename child taxons if parent name or permalink changes
# @update_children = true if params[:taxon][:name] != @taxon.name || params[:taxon][:permalink] != @taxon.permalink
# end
# 
# def update_after
# #rename child taxons
# if @update_children
# @taxon.descendants.each do |taxon|
# taxon.reload
# taxon.set_permalink
# taxon.save!
# end
# end
# end
# 
  def load_permalink_part
    @permalink_part = object.permalink.split("/").last
  end
end
