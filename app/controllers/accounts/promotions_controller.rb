class Accounts::PromotionsController <  Accounts::BaseController
  before_filter :check_promotions_limits, :only => [:new, :create]

  def index
    @promotions = current_user.promotions

    respond_to do |format|
      format.html # index.html.erb
      format.json  { render :json => @promotions }
    end
  end


  def show
    @promotion = current_user.promotions.find_by_permalink!(params[:id])

    respond_to do |format|
      format.html # show.html.erb
      format.json  { render :json => @promotion }
    end
  end

  # GET /promotions/new
  # GET /promotions/new.xml
  def new
    @promotion = Promotion.new
    respond_to do |format|

      format.html {render :action => :new, :layout => !request.xhr?}
      #  format.html # new.html.erb
      format.json  { render :json => @promotion}
    end

  end

  # GET /promotions/1/edit
  def edit
    @promotion = current_user.promotions.find_by_permalink!(params[:id])
    if @promotion.started?
      respond_to do |format|
        format.html { redirect_to(accounts_promotions_url) }
        format.json  { head :ok }
      end
      return
    end
  end

  # POST /promotions
  # POST /promotions.xml
  def create
    @promotion = current_user.promotions.new(params[:promotion])

    respond_to do |format|
      if @promotion.save
        format.html { redirect_to accounts_promotions_path( :notice => t(:promotion_created_successfully)) }
        #    format.json  { render :json => @location, :status => :created, :location => @location }
      else
        format.html { render :action => "new" }
        format.json  { render :json => @promotion.errors, :status => :unprocessable_entity }
      end
    end
  end

  # PUT /promotions/1
  # PUT /promotions/1.xml
  def update
    @promotion = current_user.promotions.find_by_permalink!(params[:id])
    if @promotion.started?
      respond_to do |format|
        format.html { redirect_to(accounts_promotions_url) }
        format.json  { head :ok }
      end
      return
    end
    respond_to do |format|
      if @promotion.update_attributes(params[:promotion])
        format.html { redirect_to(accounts_promotion_path(@promotion), :notice => 'promotion was successfully updated.') }
        format.json  { head :ok }
      else
        format.html { render :action => "edit" }
        format.json  { render :xml => @promotion.errors, :status => :unprocessable_entity }
      end
    end
  end

  # DELETE /promotions/1
  # DELETE /promotions/1.xml
  def destroy
    @promotion = current_user.promotions.find_by_permalink!(params[:id])
    if @promotion.started?
      respond_to do |format|
        format.html { redirect_to(accounts_promotions_url) }
        format.json  { head :ok }
      end
      return
    end
    @promotion.destroy

    respond_to do |format|
      format.html { redirect_to(accounts_promotions_url) }
      format.json  { head :ok }
    end
  end

  private
  def check_promotions_limits
    unless current_user.special?
      if current_user.promotions.size > 1
        flash[:alert] = t(:reached_max_promotions)
        redirect_to accounts_promotions_path
      end
    end
  end


end
