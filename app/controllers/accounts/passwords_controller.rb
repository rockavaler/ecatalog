class Accounts::PasswordsController < Accounts::BaseController
  def new
  end
  def create
    user = User.find_by_email(params[:email])

    if user
      user.send_password_reset
      redirect_to log_in_url, :notice=> t(:password_reset_sent)
    else
      redirect_to log_in_url,:alert => "Nu exista un utilizator inregistrat cu acest email!"
    end

  end
  def edit
    @user = User.find_by_login_token!(params[:id])
  end
  def update
    @user = User.find_by_login_token!(params[:id])
    if @user.login_token_generated_at  < 2.hours.ago
      redirect_to new_accounts_password_path, :alert => "Linkul a expirat."
    elsif @user.update_attributes(params[:user])
      redirect_to log_in_url, :notice => t(:password_reset_success)
    else
      render :edit
    end
  end
end
