class TaxonsController < ApplicationController

 def index
   @taxons = Taxon.root.children
   @articles = Article.new_arrivals.paginate(:per_page=>12, :page=>params[:page])
 end

   def show
    @taxon = Taxon.find_by_permalink(params[:id])

    return unless @taxon

    @searcher = Ecatalog::Config.searcher_class.new(params.merge(:taxon => @taxon.id  ))
    @articles = @searcher.retrieve_articles
    puts @articles.count.to_s + "####################Controlles"
    respond_to do |format|
      format.html # show.html.erb
      format.json  { render :json => @taxon }
    end
   end

 private
       def accurate_title
         @taxon ? @taxon.name : super
       end
end
