class UsersController < ApplicationController
  def index
    @users = User.activated.paginate(  :per_page => 24, :page => params[:page])
  end

  def show
    @user = User.activated.find_by_permalink(params[:id])
    @articles = @user.articles.paginate(  :per_page => 12, :page => params[:page])
    @locations = @user.locations
        respond_to do |format|
          format.html # show.html.erb
          format.json  { render :json => @user }
        end
  end

end
