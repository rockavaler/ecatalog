class TicketMailer < ActionMailer::Base
  default :from => "eCatalog.md <noreply@ecatalog.md>"
  def registration_confirmation(ticket,promotion)
    @ticket = ticket
    @promotion = promotion
    mail(:to => @ticket.email, :subject => "Confirmare inregistrare")
  end
  def winner_notification(ticket,promotion)
    @ticket = ticket
    @promotion = promotion
    mail(:to => @ticket.email, :subject => "Felicitari castigator")
  end
end
