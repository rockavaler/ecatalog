class UserMailer < ActionMailer::Base
  default :from => "noreply@ecatalog.md"


  def activation_email(user)
    @user = user
    mail(:to => @user.email, :subject => "Activare")
  end

  def password_reset(user)
    @user = user
    mail(:to => @user.email, :subject => "Resetare parola")
  end

end
