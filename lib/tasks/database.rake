namespace :db do
    desc "Create some users with all the staff "
    START_ID = 51
    END_ID = 52
    task :generate_users => :environment do
      for i in START_ID..END_ID
        #create user
        u = User.new
        u.description= "This is the description for the enterprise #{i}"
        u.name= "Enterprise #{i}"
        u.email= "enterprise#{i}@example.com"
        u.password= "abc123"
        u.idno="#{i}99112233"
        u.permalink= "enterprise-#{i}"
        u.contact_person= "contact person #{i}"
        u.save
        #create Locations
        nbLocations = Random.rand(1..2)
        for j in 1..nbLocations
          l = Location.new
          l.name="Location #{i}"
          l.user= u
          l.city_id= 2
          l.save

           #Create Articles
             nbArticles =  Random.rand(1..2)
             for k in 1..nbArticles
               a = Article.new
               a.name = "Article #{i}_#{j}_#{k}"
               a.description= "Aceasta este descrierea articolului  #{i}_#{j}_#{k}"
               a.price= ( Random.rand(100))
               a.permalink= "article-#{i}-#{j}-#{k}"
               a.locations<<l
               a.user= u
               a.taxons<<Taxon.find(Random.rand(1..169) )
               a.available_on= Date.today.at_beginning_of_day
               a.save
             end
        end
      end
    end
  end