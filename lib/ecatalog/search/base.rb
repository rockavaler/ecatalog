module Ecatalog::Search
  class Base
    attr_accessor :properties

    def initialize(params)
      @properties = {}
      prepare(params)
    end

    def retrieve_articles
      base_scope = get_base_scope
      @articles_scope = @article_group.apply_on(base_scope)
      curr_page = manage_pagination && keywords ? 1 : page

      @articles = @articles_scope.paginate({
         # :include  => [:images, :master],
          :per_page => per_page,
          :page     => curr_page
        })

      # @articles = Article.all

     # puts @articles.count.to_s + "articole"
    end

    def method_missing(name)
      @properties[name]
    end


    protected
    def get_base_scope
      base_scope = @cached_article_group ? @cached_article_group.articles.active : Article.active

      #base_scope =    Article.not_deleted
      #
      #puts "Articlole base scope not deleted"+base_scope.count.to_s
      #
      # base_scope =    Article.available
      #
      #puts "Articlole base scope not available"+base_scope.count.to_s
      #
      #base_scope =    Article.active
      #
      #puts "Articlole base scope active"+base_scope.count.to_s
      #
      # base_scope = Article.all
      #  puts "Articlole base scope all"+base_scope.count.to_s
       #   puts taxon.to_s + "Taxon"
      base_scope = base_scope.in_taxon(taxon) unless taxon.blank?
      base_scope = get_articles_conditions_for(base_scope, keywords) unless keywords.blank?

    #  base_scope = base_scope.on_hand unless Ecatalog::Config[:show_zero_stock_products]
      base_scope = base_scope.group_by_articles_id if @article_group.article_scopes.size > 1
      base_scope
    end

    # method should return new scope based on base_scope
    def get_articles_conditions_for(base_scope, query)
      base_scope.like_any([:name, :description], query.split)
    end

    def prepare(params)
      @properties[:taxon] = params[:taxon].blank? ? nil : Taxon.find(params[:taxon])
      @properties[:keywords] = params[:keywords]

      per_page = 12#params[:per_page].to_i
      @properties[:per_page] = per_page > 0 ? per_page : Ecatalog::Config[:products_per_page]
      @properties[:page] = (params[:page].to_i <= 0) ? 1 : params[:page].to_i

   if !params[:order_by_price].blank?
        @article_group = ArticleGroup.new.from_route([params[:order_by_price]+"_by_master_price"])
      elsif params[:product_group_name]
        @cached_product_group = ArticleGroup.find_by_permalink(params[:article_group_name])
        @article_group = ArticleGroup.new
      elsif params[:product_group_query]
        @article_group = ArticleGroup.new.from_route(params[:article_group_query].split("/"))
      else
        @article_group = ArticleGroup.new
      end
      @article_group = @article_group.from_search(params[:search]) if params[:search]

    end
  end
end
