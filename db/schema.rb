# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended to check this file into your version control system.

ActiveRecord::Schema.define(:version => 20130217192052) do

  create_table "article_groups", :force => true do |t|
    t.string "name"
    t.string "permalink"
    t.string "order"
  end

  add_index "article_groups", ["name"], :name => "index_article_groups_on_name"
  add_index "article_groups", ["permalink"], :name => "index_article_groups_on_permalink"

  create_table "article_groups_articles", :id => false, :force => true do |t|
    t.integer "article_group_id"
    t.integer "article_id"
  end

  add_index "article_groups_articles", ["article_group_id"], :name => "index_article_groups_articles__on_article_group_id"
  add_index "article_groups_articles", ["article_id"], :name => "index_article_groups_articles_on_article_id"

  create_table "article_scopes", :force => true do |t|
    t.integer "article_group_id"
    t.string  "name"
    t.text    "arguments"
  end

  add_index "article_scopes", ["article_group_id"], :name => "index_article_scopes_on_article_group_id"
  add_index "article_scopes", ["name"], :name => "index_article_scopes_on_name"

  create_table "articles", :force => true do |t|
    t.string   "name"
    t.text     "description"
    t.decimal  "price"
    t.integer  "user_id"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "permalink"
    t.datetime "available_on"
    t.datetime "deleted_at"
    t.string   "meta_description"
    t.string   "meta_keywords"
    t.datetime "published_at"
    t.string   "title"
  end

  add_index "articles", ["available_on"], :name => "index_articles_on_available_on"
  add_index "articles", ["deleted_at"], :name => "index_articles_on_deleted_at"
  add_index "articles", ["name"], :name => "index_articles_on_name"
  add_index "articles", ["permalink"], :name => "index_articles_on_permalink"
  add_index "articles", ["user_id"], :name => "index_articles_on_user_id"

  create_table "articles_locations", :id => false, :force => true do |t|
    t.integer "article_id"
    t.integer "location_id"
  end

  add_index "articles_locations", ["article_id"], :name => "index_articles_locations_on_article_id"
  add_index "articles_locations", ["location_id"], :name => "index_articles_locations_on_location_id"

  create_table "articles_taxons", :id => false, :force => true do |t|
    t.integer "article_id"
    t.integer "taxon_id"
  end

  add_index "articles_taxons", ["article_id"], :name => "index_articles_taxons_on_article_id"
  add_index "articles_taxons", ["taxon_id"], :name => "index_articles_taxons_on_taxon_id"

  create_table "assets", :force => true do |t|
    t.integer  "viewable_id"
    t.string   "viewable_type",           :limit => 50
    t.string   "attachment_content_type"
    t.string   "attachment_file_name"
    t.integer  "attachment_size"
    t.integer  "position"
    t.string   "type",                    :limit => 75
    t.datetime "attachment_updated_at"
    t.integer  "attachment_width"
    t.integer  "attachment_height"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.text     "alt"
  end

  create_table "extractions", :force => true do |t|
    t.integer  "promotion_id"
    t.integer  "ticket_id"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "extractions", ["promotion_id"], :name => "index_extractions_promotion_id"
  add_index "extractions", ["ticket_id"], :name => "index_extractions_ticket_id"

  create_table "images", :force => true do |t|
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "locations", :force => true do |t|
    t.string   "name"
    t.text     "description"
    t.string   "address_line1"
    t.string   "address_line2"
    t.integer  "user_id"
    t.integer  "region_id"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "postal_code"
  end

  create_table "product_groups", :force => true do |t|
    t.string "name"
    t.string "permalink"
    t.string "order"
  end

  add_index "product_groups", ["name"], :name => "index_product_groups_on_name"
  add_index "product_groups", ["permalink"], :name => "index_product_groups_on_permalink"

  create_table "product_scopes", :force => true do |t|
    t.integer "product_group_id"
    t.string  "name"
    t.text    "arguments"
  end

  add_index "product_scopes", ["name"], :name => "index_product_scopes_on_name"
  add_index "product_scopes", ["product_group_id"], :name => "index_product_scopes_on_product_group_id"

  create_table "promotions", :force => true do |t|
    t.string   "name"
    t.text     "description"
    t.string   "permalink"
    t.integer  "user_id"
    t.datetime "starts_at"
    t.datetime "ends_at"
    t.integer  "extractions_count"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "promotions", ["permalink"], :name => "index_promotions_permalink", :unique => true
  add_index "promotions", ["user_id"], :name => "index_promotions_user_id"

  create_table "regions", :force => true do |t|
    t.string   "name"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "taxonomies", :force => true do |t|
    t.string   "name",       :null => false
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "taxons", :force => true do |t|
    t.integer  "taxonomy_id",                :null => false
    t.integer  "parent_id"
    t.integer  "position",    :default => 0
    t.string   "name",                       :null => false
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "permalink"
    t.integer  "lft"
    t.integer  "rgt"
    t.integer  "depth"
  end

  add_index "taxons", ["permalink"], :name => "index_taxons_on_permalink", :unique => true

  create_table "ticket_batches", :force => true do |t|
    t.integer  "tickets_count"
    t.integer  "promotion_id"
    t.datetime "completed_at"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "pdf_file_name"
    t.string   "pdf_content_type"
    t.integer  "pdf_file_size"
    t.datetime "pdf_updated_at"
  end

  add_index "ticket_batches", ["promotion_id"], :name => "index_ticket_batches_promotion_id"

  create_table "tickets", :force => true do |t|
    t.string   "ticket_number"
    t.string   "name"
    t.string   "email"
    t.string   "phone_number"
    t.datetime "registered_at"
    t.integer  "ticket_batch_id"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "tickets", ["ticket_number"], :name => "index_tickets_ticket_number"

  create_table "users", :force => true do |t|
    t.string   "email"
    t.string   "password_hash"
    t.string   "password_salt"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "phone_number",             :limit => 8
    t.string   "url",                      :limit => 20
    t.string   "name",                     :limit => 20
    t.text     "description"
    t.string   "contact_person",           :limit => 25
    t.string   "idno",                     :limit => 13
    t.string   "login_token",              :limit => 40
    t.boolean  "activated",                              :default => false
    t.string   "permalink"
    t.datetime "login_token_generated_at"
    t.integer  "user_type",                              :default => 0
  end

  add_index "users", ["permalink"], :name => "index_users_on_permalink", :unique => true

end
