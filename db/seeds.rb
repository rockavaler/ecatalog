# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rake db:seed (or created alongside the db with db:setup).
#
# Examples:
#
#   cities = City.create([{ :name => 'Chicago' }, { :name => 'Copenhagen' }])
#   Mayor.create(:name => 'Daley', :city => cities.first)


# puts "loading file taxons.yml"
# 
 # file_to_load  = Rails.root + 'db/seed/taxons.yml'
 # taxons_list   = YAML::load( File.open( file_to_load ) )
# # 
# # puts "process taxons_list"
# # 
 # taxons_list.each_pair do |key,taxon|
   # #puts "taxon_id"+taxonomy['id']
   # t = Taxon.find_by_name(taxon['name'])
   # Taxon.create(taxon) unless t
# end

namespace :db do
  desc "Load seed fixtures (from db/fixtures) into the current environment's database."
  task :seed => :environment do
    require 'active_record/fixtures'
    Dir.glob(RAILS_ROOT + '/db/seed/*.yml').each do |file|
      Fixtures.create_fixtures('db/seed', File.basename(file, '.*'))
    end
  end
end



#require 'active_record/fixtures'
#puts "Loading regular seeds"
# Dir[Rails.root.join("db/seed", "*.{yml,csv}")].each do |file|
#   Fixtures.create_fixtures("db/seed", File.basename(file, '.*'))
# end
##
# puts "Loading #{Rails.env} seeds"
# Dir[Rails.root.join("db/seed", Rails.env, "*.{yml,csv}")].each do |file|
#   Fixtures.create_fixtures("db/seed/#{Rails.env}", File.basename(file, '.*'))
#end

require 'open-uri'

puts "loading file taxons.txt"
file_to_load  = Rails.root + 'db/seed/taxons.txt'

puts "create missing taxons "
open(file_to_load) do |taxons|
  taxons.read.each_line do |taxon|
    id,name,parent_id = taxon.chomp.split("|")

    if parent_id == "0"
      parent_id = nil
      id = 1
    end
    taxon = Taxon.find_by_id(id)
    if  taxon.nil?
      taxon =  Taxon.new(:id=>id)
      taxon.update_attributes!(:id=>id, :name=>name,:parent_id =>nil,:taxonomy_id => 1)
    end


    #Country.create!(:name => name, :code => code)
  end
end

puts "Update parent_id"
open(file_to_load) do |taxons|
  taxons.read.each_line do |taxon|
    id,name,parent_id = taxon.chomp.split("|")


    if parent_id == "0"
      parent_id = nil
      id = 1
    end
    taxon = Taxon.find_by_id(id)  || Taxon.new(:id=>id)
    taxon.update_attributes!(:id=>id, :name=>name,:parent_id => parent_id,:taxonomy_id => 1)

    #Country.create!(:name => name, :code => code)
  end
end

puts "calculating lft and rgt"
# recalculate lft rgt
    Taxon.class_eval do
      indices = {}

      left_column_name = 'lft'
      right_column_name = 'rgt'
      quoted_parent_column_name = 'parent_id'
      scope = lambda { |node|}

      set_left_and_rights = lambda do |node|
        # set left
        node[left_column_name] = indices[scope.call(node)] += 1
        # find
        where("#{quoted_parent_column_name} = ?", node).order('name  ASC').each { |n| set_left_and_rights.call(n) }
        # set right
        node[right_column_name] = indices[scope.call(node)] += 1
        node.set_permalink
        node.save!
      end

      # Find root node(s)
      where("#{quoted_parent_column_name}" => nil).order('position ASC').each do |root_node|
        # setup index for this scope
        indices[scope.call(root_node)] ||= 0
        set_left_and_rights.call(root_node)
      end
    end
puts "ok"