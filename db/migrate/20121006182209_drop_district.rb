class DropDistrict < ActiveRecord::Migration
  def self.up
    drop_table :districts
  end

  def self.down
  end
end
