class AddIndexPermalink < ActiveRecord::Migration
  def self.up
    #add_index :users, :permalink, :unique => true
    #add_index :taxons, :permalink, :unique => true
    #add_index :articles, :permalink, :unique => true

  end

  def self.down
    remove_index :users, :permalink
    remove_index :taxons, :permalink
    #remove_index :articles, :permalink
  end
end
