class AddRegion < ActiveRecord::Migration
  def self.up
    remove_column :cities, :district_id
    remove_column :cities, :postal_code
    rename_table :cities, :regions
    rename_column :locations, :city_id, :region_id
    add_column :locations, :postal_code, :string
  end

  def self.down
      add_column :cities,:district_id, :integer
      add_column :cities,:postal_code, :string
      rename_column :locations, :region_id, :city_id
      rename_table :regions, :cities
      remove_column :locations, :postal_ode
  end
end

