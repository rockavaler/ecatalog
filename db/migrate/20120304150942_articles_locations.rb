class ArticlesLocations < ActiveRecord::Migration
  def self.up

    create_table "articles_locations", :id => false, :force => true do |t|
      t.integer "article_id"
      t.integer "location_id"
    end

    add_index "articles_locations", ["article_id"], :name => "index_articles_locations_on_article_id"
    add_index "articles_locations", ["location_id"], :name => "index_articles_locations_on_location_id"


  end

  def self.down
    drop_table :articles_locations
  end
end
