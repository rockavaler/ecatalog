class UserActivation < ActiveRecord::Migration
  def self.up
    add_column :users, :login_token, :string, :limit => 40
    add_column  :users, :activated, :boolean, :default =>false
  end

  def self.down
    remove_column :users, :login_token
    remove_column :users, :activated
  end
end
