class CreateTickets < ActiveRecord::Migration
  def self.up
    create_table :tickets do |t|
      t.string :ticket_number
      t.string :name
      t.string :email
      t.string :phone_number
      t.datetime :registered_at
      t.integer :ticket_batch_id
      t.timestamps
    end
    add_index :tickets, :ticket_number, :name=> "index_tickets_ticket_number"
  end

  def self.down
    drop_table :tickets
    remove_index :tickets, :ticket_number
  end
end
