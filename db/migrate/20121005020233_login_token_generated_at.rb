class LoginTokenGeneratedAt < ActiveRecord::Migration
    def self.up
      add_column :users, :login_token_generated_at, :datetime
    end

    def self.down
      remove_column :users, :login_token_generated_at
    end
end
