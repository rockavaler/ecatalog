class CreateArticles < ActiveRecord::Migration
	def self.down
		drop_table :articles
	end
	
	def self.up

		create_table :articles do |t|
			t.string   :name
			t.text     :description
			t.decimal  :price
			t.integer  :user_id
			t.datetime :created_at
			t.datetime :updated_at
			t.string   :permalink
			t.datetime :available_on
			t.datetime :deleted_at
			t.string   :meta_description
			t.string   :meta_keywords
			t.datetime :published_at
			t.string   :title
			t.timestamps
		end
		  
    add_index "articles", ["available_on"], :name => "index_articles_on_available_on"
    add_index "articles", ["deleted_at"], :name => "index_articles_on_deleted_at"
    add_index "articles", ["name"], :name => "index_articles_on_name"
    add_index "articles", ["permalink"], :name => "index_articles_on_permalink"
    add_index "articles", ["user_id"], :name => "index_articles_on_user_id"
    
	end

	
end
