class CreateTaxons < ActiveRecord::Migration
  def self.up
    create_table :taxons do |t|
  
      t.integer  "taxonomy_id",                :null => false
      t.integer  "parent_id"
      t.integer  "position",    :default => 0
      t.string   "name",                       :null => false
      t.datetime "created_at"
      t.datetime "updated_at"
      t.string   "permalink"
   

      t.timestamps
    end
    
    create_table "articles_taxons", :id => false, :force => true do |t|
      t.integer "article_id"
      t.integer "taxon_id"
    end

    add_index "articles_taxons", ["article_id"], :name => "index_articles_taxons_on_article_id"
    add_index "articles_taxons", ["taxon_id"], :name => "index_articles_taxons_on_taxon_id"
    
  end

  def self.down
    drop_table :taxons
  end
end
