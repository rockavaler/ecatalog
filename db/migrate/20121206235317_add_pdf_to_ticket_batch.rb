class AddPdfToTicketBatch < ActiveRecord::Migration
  def self.up
    add_column :ticket_batches, :pdf_file_name, :string
    add_column :ticket_batches, :pdf_content_type, :string
    add_column :ticket_batches, :pdf_file_size, :integer
    add_column :ticket_batches, :pdf_updated_at, :datetime
  end

  def self.down
    remove_column :ticket_batches, :pdf_file_name
    remove_column :ticket_batches, :pdf_content_type
    remove_column :ticket_batches, :pdf_file_size
    remove_column :ticket_batches, :pdf_updated_at
  end
end
