class AddDepth < ActiveRecord::Migration
  def self.up
    add_column :taxons, :depth, :integer
  end

  def self.down
    remove_column :taxons, :depth, :integer
  end

end
