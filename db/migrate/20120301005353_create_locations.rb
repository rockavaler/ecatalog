class CreateLocations < ActiveRecord::Migration
  def self.up
    create_table :locations do |t|
      t.string :name
      t.text :description
      t.string :address_line1
      t.string :address_line2
      t.integer :user_id
      t.integer :city_id

      t.timestamps
    end
  end

  def self.down
    drop_table :locations
  end
end
