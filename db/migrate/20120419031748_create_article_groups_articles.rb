class CreateArticleGroupsArticles < ActiveRecord::Migration
  def self.up
    create_table(:article_groups_articles,  :id => false, :force => true) do |t|
          t.column :article_group_id,       :integer
          t.column :article_id,  :integer
        
    end
    add_index "article_groups_articles", ["article_id"], :name => "index_article_groups_articles_on_article_id"
    add_index "article_groups_articles", ["article_group_id"], :name => "index_article_groups_articles__on_article_group_id"
  end

  def self.down
    drop_table :article_groups_articles
  end
end
