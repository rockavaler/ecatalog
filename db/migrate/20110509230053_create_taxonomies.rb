class CreateTaxonomies < ActiveRecord::Migration
  def self.up
    create_table :taxonomies do |t|

      t.string   "name",       :null => false
      t.datetime "created_at"
      t.datetime "updated_at"
      t.timestamps
    end
  end

  def self.down
    drop_table :taxonomies
  end
end
