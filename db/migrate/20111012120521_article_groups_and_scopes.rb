class ArticleGroupsAndScopes < ActiveRecord::Migration
  def self.up
    create_table(:article_groups) do |t|
      t.column :name,       :string
      t.column :permalink,  :string
      t.column :order,      :string
    end

    create_table(:article_scopes) do |t|
      t.column :article_group_id, :integer
      t.column :name,             :string
      t.column :arguments,        :text
    end

    add_index :article_groups, :name
    add_index :article_groups, :permalink
    add_index :article_scopes, :name
    add_index :article_scopes, :article_group_id
  end

  def self.down
    drop_table :article_groups
    drop_table :article_scopes
  end
end
