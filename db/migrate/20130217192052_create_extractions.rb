class CreateExtractions < ActiveRecord::Migration
  def self.up
    create_table :extractions do |t|
      t.integer :promotion_id
      t.integer :ticket_id
      t.timestamps
    end
    add_index :extractions, :promotion_id, :name=> "index_extractions_promotion_id"
    add_index :extractions, :ticket_id, :name=> "index_extractions_ticket_id"
  end

  def self.down
    drop_table :extractions
    remove_index :extractions, :promotion_id
    remove_index :extractions, :ticket_id
  end
end
