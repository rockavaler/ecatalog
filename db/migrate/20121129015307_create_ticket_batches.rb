class CreateTicketBatches < ActiveRecord::Migration
  def self.up
    create_table :ticket_batches do |t|
      t.integer :tickets_count
      t.integer :promotion_id
      t.datetime :completed_at
      t.timestamps
    end
    add_index :ticket_batches, :promotion_id , :name=> "index_ticket_batches_promotion_id"
  end

  def self.down
    drop_table :ticket_batches
    remove_index :ticket_batches, :promotion_id
  end

end
