class CreatePromotions < ActiveRecord::Migration
  def self.up
    create_table :promotions do |t|
      t.string :name
      t.text :description
      t.string :permalink
      t.integer :user_id
      t.datetime :starts_at, :ends_at
      t.integer :extractions_count
      t.timestamps
    end
    add_index :promotions, :permalink, :name=> "index_promotions_permalink", :unique=>true
    add_index :promotions, :user_id, :name=> "index_promotions_user_id"

  end

  def self.down
    drop_table :promotions
    remove_index :promotions, :permalink
    remove_index :promotions, :user_id
  end
end
