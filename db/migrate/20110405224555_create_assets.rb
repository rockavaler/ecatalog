class CreateAssets < ActiveRecord::Migration
  def self.up

    create_table :assets do |t|
      t.integer  "viewable_id"
      t.string   "viewable_type", :limit => 50
      t.string   "attachment_content_type"
      t.string   "attachment_file_name"
      t.integer  "attachment_size"
      t.integer  "position"
      t.string   "type", :limit => 75
      t.datetime "attachment_updated_at"
      t.integer  "attachment_width"
      t.integer  "attachment_height"

      t.timestamps
    end
  end

  def self.down
    drop_table :assets
  end
end
