class ChangeUsers < ActiveRecord::Migration
  def self.up
    add_column :users, :phone_number, :string, :limit => 8
    add_column :users, :url, :string, :limit => 20
    add_column :users, :name, :string, :limit => 20
    add_column :users, :description, :text
    add_column :users, :contact_person, :string, :limit => 25
    add_column :users, :idno, :string, :limit => 13

  end

  
  def self.down

    remove_column :users, :phone_number
    remove_column :users, :url
    remove_column :users, :name
    remove_column :users, :description
    remove_column :users, :contact_person
    remove_column :users, :idno
  end
end
