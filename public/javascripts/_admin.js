/**
This is a collection of javascript functions and whatnot
under the spree namespace that do stuff we find helpful.
Hopefully, this will evolve into a propper class.
**/


jQuery(document).ajaxStart(function(){
  jQuery("#progress").slideDown();
});

jQuery(document).ajaxStop(function(){
  jQuery("#progress").slideUp();
});

handle_date_picker_fields = function(){
    $('.datepicker').datepicker({
        dateFormat: 'yy/mm/dd',

        buttonImage: "/images/datepicker/cal.gif",
        buttonImageOnly: true
    });
}

$(document).ready(function(){
    handle_date_picker_fields();
});


