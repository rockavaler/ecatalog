Ecatalog::Application.routes.draw do
  get "users/index"
  get "home" => "static_pages#home"
  get "users/show"

  #resources :sessions, :only => [:create]
  get "log_out" => "accounts/sessions#destroy", :as => "log_out"
  get "log_in" => "accounts/sessions#new", :as => "log_in"
  get "sign_up" => "accounts/users#new", :as => "sign_up"

  get "index" => "accounts/articles#index", :as => "accounts_index"
  get "locations" => "accounts/locations#index", :as => "locations_index"
    get "accounts/profile" => "accounts/users#edit" , :as=>"accounts_profile"
  get "contents"=>"taxons#index", :as =>"contents"

  get "companies" => "users#index", :as => "companies"
  root :to => "static_pages#home"

  match '/contents/*id', :to => 'taxons#show', :as => :nested_taxons

  resources :taxons
  resources :users , :path => "/companies"
  resources :promotions  do
    resources :tickets   do
      member  do
        get :register
        post :register
      end

    end
  end


namespace :accounts do
  resources :passwords
  resources :users do
    member do
      get :activate_account
    end

  end

  resources :sessions
  resources :locations
  resources :articles do
    resources :images do
    end
    resources :taxons do
      member do
        get :select
        delete :remove
      end
      collection do
        post :available
        get :selected
      end
    end
  end
  resources :promotions   do
    resources :ticket_batches do
      member do
        get :download
      end
    end
    resources :extractions do
      collection do
        post :extract
      end
    end
  end


  resources :taxonomies do
    member do
      get :get_children
    end
    resources :taxons
  end
end

#resources :images

  resources :articles
# resources :images

# resources :articles do
# resources :images do
# end
# resources :taxons do
# member do
# get :select
# delete :remove
# end
# collection do
# post :available
# get  :selected
# end
# end
# end
#
# resources :taxonomies do
# member do
# get :get_children
# end
# resources :taxons
# end

end
